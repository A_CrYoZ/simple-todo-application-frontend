const { Router } = require("express");
const axios = require("axios");
const { check, validationResult } = require("express-validator");

const { API_ROUTE } = require("../../configs/api_config");

const routerDone = require("./done");

const router = new Router();

const validations = [
  check("task")
    .trim()
    .isLength({ min: 3 })
    .escape()
    .withMessage("Minimal length for task name is 3 letter!"),
];

module.exports = () => {
  router.get("/", async (request, response, next) => {
    try {
      // Get data from REST API
      const result = await axios.get(`${API_ROUTE}/tasks/incomplete`).catch((error) => {
        if (error.response.status !== 404) {
          next(error);
        }
      });

      const data = result?.data;

      const tasksListFilled = data?.length > 0;

      // Save tasks to user cookies
      request.session.tasksList = data;

      response.render("layout", { template: "index", tasksListFilled, tasksList: data });
    } catch (error) {
      next(error);
    }
  });

  router.post("/", validations, async (request, response, next) => {
    try {
      let requestError = false;
      let requestErrorObj;

      const validationErrors = validationResult(request);

      if (validationErrors.isEmpty()) {
        // Add task for user. If user does not exist - it automatically creates new user and returns list of incomplete tasks
        const result = await axios
          .post(`${API_ROUTE}/tasks/addTask`, {
            description: request.body.task,
            completed: false,
          })
          .catch((error) => {
            if (error.response.status === 400) {
              requestError = true;
              requestErrorObj = { msg: error.response.data.message };
            }
          });

        const tasksList = requestError ? request.session.tasksList : result.data.tasks;

        // Save tasks to user cookies if there's no error on request
        if (!requestError) request.session.tasksList = tasksList;

        // Re-render page for user with new tasks
        response.render("layout", {
          template: "index",
          tasksListFilled: true,
          tasksList,
          success: !requestError,
          error: requestErrorObj,
        });
      } else {
        const error = validationErrors.array()[0];

        let tasksListFilled = false;
        let tasksList = [];

        if (request.session.tasksList) {
          tasksListFilled = true;
          tasksList = request.session.tasksList;
        }

        response.render("layout", {
          template: "index",
          tasksListFilled,
          tasksList,
          error,
        });
      }
    } catch (error) {
      next(error);
    }
  });

  router.post("/api/tasks/:taskName/done", async (request, response, next) => {
    try {
      await axios.post(`${API_ROUTE}/tasks/${request.params.taskName}/done`);

      response.json({ result: "ok" });
    } catch (error) {
      next(error);
    }
  });

  router.use("/done", routerDone());

  return router;
};
